<?php

use Gregwar\Image\Image;

//Home page
$app->match('/', function() use ($app) {
    return $app['twig']->render('home.html.twig', array(
        'connected' => $app['session']->get('admin') 
    ));
})->bind('home');

//Book list Page
$app->match('/books', function() use ($app) {
    return $app['twig']->render('books.html.twig', array(
        'connected' => $app['session']->get('admin'),
        'books' => $app['model']->getBooks()
    ));
})->bind('books');

// Login page
$app->match('/admin', function() use ($app) {
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('login') && $post->has('password') &&
            array($post->get('login'), $post->get('password')) == $app['config']['admin']) {
            $app['session']->set('admin', true);
            $success = true;
        }
    }
    return $app['twig']->render('admin.html.twig', array(
        'connected' => $app['session']->get('admin'),
        'success' => $success
    ));
})->bind('admin');

//Logout page
$app->match('/logout', function() use ($app) {
    $app['session']->remove('admin');
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('logout');


// Book insertion Page
$app->match('/addBook', function() use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig', array(
            'connected' => $app['session']->get('admin')
        ));
    }

    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('title') && $post->has('author') && $post->has('synopsis') &&
            $post->has('copies')) {
            $files = $request->files;
            $image = '';

            // Resizing image
            if ($files->has('image') && $files->get('image')) {
                $image = sha1(mt_rand().time());
                Image::open($files->get('image')->getPathName())
                    ->resize(240, 300)
                    ->save('uploads/'.$image.'.jpg');
                Image::open($files->get('image')->getPathName())
                    ->resize(120, 150)
                    ->save('uploads/'.$image.'_small.jpg');
            }

            // Saving the book to database
            $app['model']->insertBook($post->get('title'), $post->get('author'), $post->get('synopsis'),
                $image, (int)$post->get('copies'));
        }
    }

    return $app['twig']->render('addBook.html.twig', array(
        'connected' => $app['session']->get('admin')
    ));
})->bind('addBook');



// Book page
$app->match('/book/{id}', function($id) use ($app) {
    return $app['twig']->render('book.html.twig', array(
        'connected' => $app['session']->get('admin'),
        'book' => $app['model']->getBook($id)[0],
        'number' => $app['model']->getNumberOfBooks($id)[0],
        'availables' => $app['model']->getAvailableBooks($id),
        'borrowed' => $app['model']->getBorrowedBooks($id)
    ));
})->bind('book');


// Borrow page
$app->match('/borrow/{id}', function($id) use ($app) {
    $request = $app['request'];
    $success = false;
    $idExemplaire = $app['model']->availableBook($id)[0];

    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ( $idExemplaire>-1 && $post->has('personne') && $post->has('fin') ){
            $app['model']->insertEmprunt($post->get('personne'), $idExemplaire, $post->get('fin'));
            $success = true;
        }
    }
    //Display de confirmation page
    return $app['twig']->render('borrow.html.twig', array(
        'session' => $app['session'],
        'connected' => $app['session']->get('admin'),
        'book' => $app['model']->getBook($id)[0],
        'idBook' => $idExemplaire,
        'success' => $success
    ));
})->bind('borrow');


// Borrow page
$app->match('/giveBack/{id}', function($id) use ($app) {
    $app['model']->giveBack($id);
    //Display de confirmation page
    return $app['twig']->render('giveBack.html.twig', array(
        'session' => $app['session'],
        'connected' => $app['session']->get('admin'),
        'id' => $id
    ));
})->bind('giveBack');
