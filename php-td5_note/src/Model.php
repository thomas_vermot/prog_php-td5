<?php

class Model
{
    protected $pdo;

    public function __construct(array $config)
    {
        try {
            if ($config['engine'] == 'mysql') {
                $this->pdo = new \PDO(
                    'mysql:dbname='.$config['database'].';host='.$config['host'],
                    $config['user'],
                    $config['password']
                );
                $this->pdo->exec('SET CHARSET UTF8');
            } else {
                $this->pdo = new \PDO(
                    'sqlite:'.$config['file']
                );
            }
        } catch (\PDOException $error) {
            throw new ModelException('Unable to connect to database');
        }
    }

    /**
     * Tries to execute a statement, throw an explicit exception on failure
     */
    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Inserting a book in the database
     */
    public function insertBook($title, $author, $synopsis, $image, $copies)
    {
        $query = $this->pdo->prepare('INSERT INTO livres (titre, auteur, synopsis, image)
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($title, $author, $synopsis, $image));

        // TODO: Créer $copies exemplaires
    }

    /**
     * Getting all the books
     */
    public function getBooks()
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres');

        $this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Getting the book with the id $id
     */
    public function getBook($id)
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres WHERE id = ? LIMIT 1');
        $query->execute(array($id));

        //$this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Getting the list of physical books
     */
    public function getPhysicalBooks($id)
    {
        $query = $this->pdo->prepare('SELECT a.id, b.borrowed 
            FROM (
                SELECT exemplaires.id AS id
                FROM exemplaires
                WHERE exemplaires.book_id = ?
            ) AS a
            LEFT JOIN (
                SELECT COUNT(*)            AS borrowed, 
                       emprunts.exemplaire AS id
                FROM emprunts
            )AS b
            ON a.id = b.id');
        $query->execute(array($id));

        //$this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Getting the number of books with the id $id
     */
    public function getNumberOfBooks($id)
    {
        $query = $this->pdo->prepare('SELECT COUNT(*) AS nb FROM exemplaires WHERE book_id = ? LIMIT 1');
        $query->execute(array($id));

        //$this->execute($query);

        return $query->fetchAll();
    }


    /**
     * Getting the number of available books with the id $id
     */
    public function getAvailableBooks($id)
    {
        $query = $this->pdo->prepare('SELECT * 
            FROM exemplaires 
            WHERE (
                SELECT COUNT(*) 
                FROM emprunts 
                WHERE exemplaire = exemplaires.id
                AND fini = 0
            ) = 0 
            AND exemplaires.book_id = ?'
        );
        $query->execute(array($id));

        //$this->execute($query);
        return $query->fetchAll();
    }
    /**
     * Getting the number of borrowed books with the id $id
     */
    public function getBorrowedBooks($id)
    {
        $query = $this->pdo->prepare('SELECT * 
            FROM exemplaires 
            WHERE (
                SELECT COUNT(*) 
                FROM emprunts 
                WHERE exemplaire = exemplaires.id
                AND fini = 0
            ) > 0 
            AND exemplaires.book_id = ?'
        );
        $query->execute(array($id));

        //$this->execute($query);
        return $query->fetchAll();

    }


    /**
     * Checking if a book exists
     */
    public function BookExists($idBook)
    {
        $query = $this->pdo->prepare('SELECT COUNT(*) AS nb FROM exemplaires WHERE book_id = ? LIMIT 1');
        $query->execute(array($idBook));
        $number = $query->fetchAll();
        //$this->execute($query);
        return sizeof($number);
    }


    /**
     * Checking if a book is borrowed
     */
    public function availableBook($idBook)
    {
        $result = -1;
        if($this->BookExists($idBook)){

            $query = $this->pdo->prepare('SELECT exemplaires.id
                FROM exemplaires 
                WHERE (
                    SELECT COUNT(*) 
                    FROM emprunts 
                    WHERE exemplaire = exemplaires.id
                ) = 0 
                AND exemplaires.book_id = 5
                LIMIT 1');
            $query->execute(array($idBook));
            $result = $query->fetch();
        }
        return $result;
    }


    /**
     * Inserting a book in the database
     */
    public function insertEmprunt($personne, $exemplaire, $fin)
    {
        $debut = new DateTime();
        //fin = new DateTime();
        //$fin = date_add($debut, date_interval_create_from_date_string('7 days') );
        $query = $this->pdo->prepare('INSERT INTO emprunts (personne, exemplaire, debut, fin)
            VALUES (:personne, :exemplaire, :debut, :fin)');
        $this->execute($query, array(
            ':personne' => $personne, 
            ':exemplaire' => $exemplaire, 
            ':debut' => $debut->format('Y-m-d H:i:s'), 
            ':fin' => $fin
        ));

        // TODO: Créer $copies exemplaires
    }
    public function giveBack($idExemplaire)
    {
        $query=$this->pdo->prepare("UPDATE emprunts 
            SET fini = 1 
            WHERE exemplaire = :id
            AND fini = 0");
        $query->execute(array(
            ':id' => $idExemplaire
        ));
    }
}
